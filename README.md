# es_hbase

#### 项目介绍
##### 节点分配
##### [搭建步骤参考-点我](https://blog.csdn.net/dkjhl/article/details/80191504)
| 集群搭建 | NN | DN | ZK | ZKFC | JN | RM | NM(任务管理) |
| -------- | -- | -- | -- | ---- | -- | -- | ------------ |
| Hadoop1  | Y  |    | Y  | Y    |    | Y  |              |
| Hadoop2  | Y  | Y  | Y  | Y    | Y  | Y  | Y            |
| Hadoop3  |    | Y  | Y  |      | Y  |    | Y            |
| Hadoop4  |    | Y  |    |      | Y  |    | Y            |

项目启动流程：

    1、安装对应节点
        hadoop节点： hadoop1， hadoop2， hadoop3， hadoop4
        zookeeper节点：  hadoop1--hadoop3
        hbase节点： hadoop1-hadoop4
        
    2、启动流程顺序（针对所有节点hadoop1--hadoop4）
        关闭防火墙：            service iptables stop
        时间同步：              ntpdate 0.asia.pool.ntp.org
        启动zookeeper：         zkServer.sh start
    
    3、启动Hadoop集群
        hadoop1    start-all.sh
        
    4、启动hbase集群
        hadoop1    start-hbase.sh

        四个节点均可进入HBASE数据库
        hbase shell
        建表语句 create 'doc','cf1'
        删除表数据 truncate 'doc'

    4、所有节点启动elasticsearch
        cd /home/elasticsearch
        su bigdata
        $ bin/elasticsearch

        删除索引库
        [root@hadoop1 home]# curl -XDELETE 'hadoop1:9200/dkjhl'
        创建索引库
        [root@hadoop1 home]# curl -XPOST 'hadoop1:9200/dkjhl' -d @dkjhl.json
        
    5、hadoop1节点启动kibana
        cd /home/kibana-4.4.1-linux-x64
        bin/kibana
    
    6、项目启动流程
        maven项目：     端口在pom.xml文件中修改
        访问地址：      localhost:8395
        输入关键字：    关键字在resources目录下的article.txt中


项目截图

![输入图片说明](https://gitee.com/uploads/images/2018/0524/081404_7b17b90f_954087.png "664696327917713104.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0524/081355_83fd5c2a_954087.png "494450879479579151.png")

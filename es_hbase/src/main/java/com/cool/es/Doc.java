package com.cool.es;



/**
 * 文章实体类
 * @author Administrator
 *
 */

/**
 * 架构思路：
 * 数据存储在HBASE里面
 * 将HBASE里面的数据rowkey+数据建立索引
 * 
 * @author root
 *
 */
public class Doc {
	
	private Integer id;// rowkey
	
	private String title;
	
	private String describe;
	
	private String content;
	
	private String author;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}

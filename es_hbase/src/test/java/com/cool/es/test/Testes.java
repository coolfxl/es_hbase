package com.cool.es.test;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.junit.Test;

import com.cool.es.Doc;

/**
 * Created by root on 2016/3/26 0026.
 */
public class Testes {
	public static void main(String[] args) throws Exception {
		Settings settings = Settings.settingsBuilder()
				.put("cluster.name", "chenkl").build();
		Client client = TransportClient
				.builder()
				.settings(settings)
				.build()
				.addTransportAddress(
						new InetSocketTransportAddress(InetAddress.getByName("hadoop1"), 9300))
				.addTransportAddress(
						new InetSocketTransportAddress(InetAddress.getByName("hadoop2"), 9300))
				.addTransportAddress(
						new InetSocketTransportAddress(InetAddress.getByName("hadoop3"), 9300))
				.addTransportAddress(
						new InetSocketTransportAddress(InetAddress.getByName("hadoop4"), 9300));

		XContentBuilder builder = XContentFactory.jsonBuilder()
				.startObject()
				.field("name", "man in black")
				.field("age", "20")
				.field("hobby", "music")
				.endObject();
		String json = builder.string();
		// 第一步建立索引，建立结束后注释
//		IndexResponse response0 = client.prepareIndex("coolfxl", "cool", "1").setSource(json).execute().actionGet();
		
		// 查询索引的两种方式
//		 GetResponse response1 = client.prepareGet("coolfxl", "cool","2").execute().actionGet();
//		 GetResponse response2 = client.prepareGet("bjsxt", "emp", "2").get();
//		 Map<String, Object> source = response2.getSource();
//		 for(Map.Entry<String,Object> filed :source.entrySet()){
//			 String key = filed.getKey();
//		 	System.out.println("key===="+key+"    value====="+filed.getValue().toString());
//		 }
		 
		// 修改 update，更新索引
//		 UpdateRequest updateRequest = new UpdateRequest();
//		 updateRequest.index("bjsxt");
//		 updateRequest.type("emp");
//		 updateRequest.id("2");
//		 updateRequest.doc(builder);
//		 client.update(updateRequest).get();
		 
		// 删除索引
		 DeleteResponse response4 = client.prepareDelete("bjsxt", "doc","1").get();
		 
		 // 搜索api
		 // prepareSearch是一个参数不定长的方法，可以传递任意多个库
//		SearchResponse response = client.prepareSearch("bjsxt", "coolfxl").setTypes("emp", "cool")
//				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//				// Query
//				.setQuery(QueryBuilders.termQuery("user", "9999999"))
//				.setQuery(QueryBuilders.rangeQuery("age").from(12).to(25))
////				.setPostFilter(QueryBuilders.rangeQuery("age").from(12).to(25))
//				 // Filter
//				.setFrom(0).setSize(60).setExplain(true).execute().actionGet();
//
//		SearchHits hits = response.getHits();
//		System.out.println(hits.getHits().length);
//		SearchHit[] hits1 = hits.getHits();
//		for (SearchHit hit : hits1) {
//			Map<String, Object> source = hit.getSource();
//			for (Map.Entry<String, Object> filed : source.entrySet()) {
//				String key = filed.getKey();
//				System.out.println("key====" + key + "    value====="
//						+ filed.getValue().toString());
//			}
//		}
		
		
	}
	
//	@Test
//	public void testChaos() throws Exception {
//		List<Doc> arrayList = new ArrayList<Doc>();
//		File file = new File("E:\\cool\\article.txt"); 
//        List<String> list = FileUtils.readLines(file,"UTF-8");
//        for(String line : list){
//        	// 封装数据
//            Doc Doc = new Doc();
//            String[] split = line.split("\t");
//            int parseInt = Integer.parseInt(split[0].trim());
//            Doc.setId(parseInt);
//            Doc.setTitle(split[1]);
//            Doc.setAuthor(split[2]);
//            Doc.setDescribe(split[3]);
//            Doc.setContent(split[3]);
//            arrayList.add(Doc);
//        }
//        
//        for (Doc doc : arrayList) {
//			System.out.println(doc.getDescribe());
//		}
//	}
}
